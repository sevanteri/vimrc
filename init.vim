set nocompatible

let mapleader = "\<space>"
let maplocalleader = "-"

" set timeoutlen=250
set ttimeout
set ttimeoutlen=0

filetype on
"set encoding=utf-8

set path+=**

set number
set relativenumber

set showcmd
set autoread
set signcolumn=yes

" statusline {{{
set laststatus=2
set statusline=%f\ %m%r
set statusline+=%=
set statusline+=ft=%Y
set statusline+=\ \ col:%2c\ %5l/%L
" }}}

set updatetime=300
let g:cursorhold_updatetime = 300

set mouse=a
if !has('nvim')
    set ttymouse=sgr
    set t_Co=256
    set ttyfast
else
    "let $NVIM_TUI_ENABLE_TRUE_COLOR=1
endif


set noshowmode
set nojoinspaces " Prevents inserting two spaces after punctuation on a join (J)

set hidden
set cursorline
set colorcolumn=120

"set hlsearch
set incsearch

set noequalalways
set splitright
set diffopt+=vertical

set scrolloff=2
set list listchars=tab:├─,trail:·,nbsp:·
set backspace=indent,eol,start
set fileformats="unix,dos,mac"
set lazyredraw

set shiftwidth=4
set tabstop=4
set softtabstop=4
"set smartindent
set autoindent
set smarttab
set expandtab
set ignorecase
set smartcase

set formatoptions-=o

" set foldmethod=manual
" set nofoldenable
set foldnestmax=20
set foldlevelstart=20

set wildmenu
set wildmode=longest:full,full

set completeopt-=preview
set completeopt+=noselect,menuone
set completeopt+=noinsert

" better wrap visuals
set breakindent
set breakindentopt=shift:4
set showbreak=>

" let g:loaded_matchparen=1
" set noshowmatch

if exists('&inccommand')
    set inccommand=split
endif

" reset syntax on window focus (probably not needed anymore with TS)
" autocmd FocusGained * :syntax sync fromstart

" gui
set guioptions-=r
set guioptions-=T

" ============
" mapping keys
" ============
nnoremap <leader>w :w<cr>
" nnoremap <leader>l :!love .<cr>
" nnoremap <leader>c :w !tcc -run - \|aplay<cr>

" expression register eval selection
vnoremap <leader>e c<c-r>=<c-r>"<cr><esc>

" <count> ctrl-6 to jump to <count> buffer
nnoremap <silent> <C-^> :<C-u>exe v:count ? v:count . 'b' : 'b' . (bufloaded(0) ? '#' : 'n')<CR>

" reselect latest changed, put or yanked text
nnoremap gV `[v`]

vmap <Leader>y "+y
vmap <Leader>d "+d
nmap <Leader>p "+p
nmap <Leader>P "+P
vmap <Leader>p "+p
vmap <Leader>P "+P

nnoremap <M-j> <C-W><C-J>
nnoremap <M-k> <C-W><C-K>
nnoremap <M-l> <C-W><C-L>
nnoremap <M-h> <C-W><C-H>
if has('nvim')
    " leave nvim terminal with alt+[hjkl]
    tnoremap <M-h> <C-\><C-n><C-w>h
    tnoremap <M-j> <C-\><C-n><C-w>j
    tnoremap <M-k> <C-\><C-n><C-w>k
    tnoremap <M-l> <C-\><C-n><C-w>l
    autocmd TermOpen setlocal signcolumn="no"
endif

nnoremap <expr> j v:count ? (v:count > 5 ? "m'" . v:count : '') . 'j' : 'gj'
nnoremap <expr> k v:count ? (v:count > 5 ? "m'" . v:count : '') . 'k' : 'gk'

nnoremap <ScrollWheelUp>     <C-Y>
nnoremap <ScrollWheelDown>   <C-E>
nnoremap <S-ScrollWheelUp>     <C-Y>
nnoremap <S-ScrollWheelDown>   <C-E>

"smart home
" noremap <expr> <silent> 0 col('.') == match(getline('.'),'\S')+1 ? '0' : '^'
" noremap <expr> <silent> H col('.') == match(getline('.'),'\S')+1 ? '0' : '^'
" noremap L $
nnoremap ¨ ^

" smart h and 0, fold if trying to move past start of line
" nnoremap <expr> <silent> h col('.') == 1 ? ':silent! norm za<cr>' : 'h'
" nnoremap <expr> <silent> 0 col('.') == 1 ? ':silent! norm za<cr>' : '0'

nnoremap Q @q

nnoremap <silent> <leader><leader> :nohlsearch\|cclose<cr>

" move lines up and down
nnoremap <M-S-j> :m .+1<CR>==
nnoremap <M-S-k> :m .-2<CR>==
vnoremap <M-S-j> :m '>+1<CR>gv=gv
vnoremap <M-S-k> :m '<-2<CR>gv=gv

" speed up viewport scrolling
nnoremap <C-e> 2<C-e>
nnoremap <C-y> 2<C-y>

" resize splits with alt+</>
nnoremap <M-lt> 5<C-w><lt>
nnoremap <M-char-62> 5<C-w><char-62>
" and alt+ and alt-
nnoremap <M-+> 5<C-w>+
nnoremap <M--> 5<C-w>-
nnoremap <M-=> <C-w>=


nmap ö [
nmap ä ]
omap ö [
omap ä ]
xmap ö [
xmap ä ]

" trailing whitespace cleaner
nnoremap <leader>W :%s/\s\+$//<cr>:let @/=''<CR>

vnoremap . :norm.<CR>

nnoremap <leader>so :source %<CR>

" search and replace word */#
nnoremap c*  *Ncgn
nnoremap c#  #NcgN
" append after word
nnoremap c>* *Ncgn<C-r>"
nnoremap c># #NcgN<C-r>"
" prepend before word
nnoremap c<* *Ncgn<C-r>"<S-Left>
nnoremap c<# #NcgN<C-r>"<S-Left>

" Allow saving of files as sudo when I forgot to start vim using sudo.
cmap W!! w !sudo tee > /dev/null %

set sessionoptions-=options    " do not store global and local values in a session
"set sessionoptions-=folds      " do not store folds

if has('nvim')
    "let g:python_host_prog = '/home/sevanteri/.neovim_venv2/bin/python'
    let g:python3_host_prog = '/home/sevanteri/.neovim_venv/bin/python'
endif

" ================ Persistent Undo ==================
" Keep undo history across sessions, by storing in file.
" Only works all the time.
if has('persistent_undo')
    silent !mkdir ~/.vim/backups > /dev/null 2>&1
    set undodir=~/.vim/backups
    set undofile
endif

" Backups
" ================ Turn Off Swap Files ==============
set noswapfile
set nobackup
set nowb

" ============ Other settings (plugins) =============
for f in split(glob('~/.vim/settings/*.vim'), '\n')
    exe 'source' f
endfor

lua require('init')
