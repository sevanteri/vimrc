
" syntax keyword qmk_trans _______
" highlight def link qmk_trans Comment
" syntax match qmk_key '[a-zA-Z0-9]\+' contained

"syntax region myctest contains=qmk_key start="+|+" end="+|+" extend oneline
"highlight def link myctest Constant

set foldmethod=marker
set tabstop=4
set shiftwidth=4
set expandtab
