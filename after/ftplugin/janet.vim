
command! -nargs=0 StartJanetRepl
  \ call jobstart("janet -e '(import spork/netrepl) (netrepl/server)'")

command! -nargs=0 StartLocalJanetRepl
  \ call jobstart("jpm -l janet -e '(import spork/netrepl) (netrepl/server)'")

nnoremap <buffer> <localleader>sj :StartJanetRepl<cr>
nnoremap <buffer> <localleader>slj :StartLocalJanetRepl<cr>

