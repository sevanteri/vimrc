
setlocal foldmethod=indent
"setlocal foldtext=substitute(getline(v:foldstart),'\\t','\ \ \ \ ','g')
"

" one small conceal just for fun
syntax keyword pyNiceStatement lambda conceal cchar=λ
hi link pyNiceStatement Statement
setlocal conceallevel=1


nmap <silent> <buffer> <localleader>jC :JupyterConnect<cr>
" Run current file
nnoremap <buffer> <silent> <localleader>jR :JupyterRunFile<CR>
nnoremap <buffer> <silent> <localleader>jI :PythonImportThisFile<CR>

" Change to directory of current file
nnoremap <buffer> <silent> <localleader>jd :JupyterCd %:p:h<CR>

" Send a selection of lines
nnoremap <buffer> <silent> <localleader>jx :JupyterSendCell<CR>
nnoremap <buffer> <silent> <localleader>jE :JupyterSendRange<CR>
nmap     <buffer> <silent> <localleader>je <Plug>JupyterRunTextObj
vmap     <buffer> <silent> <localleader>je <Plug>JupyterRunVisual

" nnoremap <buffer> <silent> <localleader>jU :JupyterUpdateShell<CR>

" Debugging maps
nnoremap <buffer> <silent> <localleader>jb :PythonSetBreak<CR>

command! -nargs=0 RunConsole vert sp | term jupyter-console $(get_jupyter_kernel foo)

command! -nargs=0 RunQtConsole
  \ call jobstart("jupyter-qtconsole --JupyterWidget.include_other_output=True $(get_jupyter_kernel foo)")

nnoremap <buffer> <silent> <localleader>jqt :RunQtConsole<cr>
nnoremap <buffer> <silent> <localleader>jco :RunConsole<cr>

" semshi
" nnoremap <buffer> <silent> <localleader>se :Semshi error<CR>
" nnoremap <buffer> <silent> <localleader>sge :Semshi goto error<CR>
" nnoremap <buffer> <silent> <localleader>sren :Semshi rename<CR>
" nnoremap <buffer> <silent> <M-S-h> :Semshi goto name prev<CR>
" nnoremap <buffer> <silent> <M-S-l> :Semshi goto name next<CR>



runtime zepl/contrib/python.vim

autocmd! FileType python let b:repl_config = {
            \   'cmd': 'ipython --no-autoindent',
            \   'formatter': function('zepl#contrib#python#formatter')
            \ }


