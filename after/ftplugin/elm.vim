
syntax match elmLambda "\\\(.\+ \)\+->"he=s+1 conceal cchar=λ
hi link elmLambda Function
hi! link Conceal elmLambda
setlocal conceallevel=1
setlocal concealcursor=ni

