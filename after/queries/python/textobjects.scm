; extends

(assignment right: (_) @expression.outer)
(keyword_argument value: (_) @expression.outer)

((argument_list
  .
  (_) @expression.outer
  .
  ","?))
((argument_list
  "," @_start
  .
  (_) @expression.outer))

(decorator) @expression.outer

(if_statement condition: (_) @expression.outer)
(boolean_operator left: (_) @expression.outer)
(boolean_operator right: (_) @expression.outer)
(comparison_operator (_) @expression.outer)

(return_statement (_) @expression.outer)
(expression_statement (_) @expression.outer)

