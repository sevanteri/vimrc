let g:loaded_matchit = 1
" load vim-plug
if empty(glob("~/.vim/autoload/plug.vim"))
  execute '!curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.github.com/junegunn/vim-plug/master/plug.vim'
  autocmd VimEnter * PlugInstall
  source ~/.vimrc
endif

let hostname = substitute(system('hostname'), '\n', '', '')

call plug#begin('~/.vim/bundle')

" Add or remove your Bundles here:

Plug 'gruvbox-community/gruvbox'
" Plug 'ellisonleao/gruvbox.nvim'

" Plug 'junegunn/fzf' , { 'dir': '~/.fzf', 'do': 'yes \| ./install' }
" Plug 'junegunn/fzf.vim'

" Plug 'tpope/vim-commentary'
Plug 'tpope/vim-repeat'
" Plug 'tpope/vim-unimpaired'

Plug 'machakann/vim-sandwich'

Plug 'wellle/targets.vim', {'on': []}
augroup LoadDuringHold_Targets
    autocmd!
    autocmd CursorHold,CursorHoldI * call plug#load('targets.vim') | autocmd! LoadDuringHold_Targets
augroup end

Plug 'machakann/vim-textobj-delimited'
Plug 'michaeljsmith/vim-indent-object'

Plug 'unblevable/quick-scope'

Plug 'tpope/vim-fugitive'


if hostname =~ "^work"
    " Plug 'othree/html5.vim', {'for': ['html', 'tk']}

    Plug 'kana/vim-textobj-user'

    Plug 'PotatoesMaster/i3-vim-syntax', {'for': 'i3'}
    Plug 'tmux-plugins/vim-tmux'

    Plug 'junegunn/gv.vim'

    " Plug 'elixir-editors/vim-elixir'

    Plug 'vito-c/jq.vim'

    Plug 'rust-lang/rust.vim', {'for': 'rust'}
    " rust-tools fork
    " Plug 'mrcjkb/rustaceanvim'

    " Plug 'bps/vim-textobj-python'
    Plug 'Vimjas/vim-python-pep8-indent'
    Plug 'fisadev/vim-isort', {'for': 'python'}

    Plug 'machakann/vim-swap'

    Plug 'cespare/vim-toml', {'for': 'toml'}

    Plug 'tpope/vim-dadbod'
    Plug 'kristijanhusak/vim-dadbod-completion'
    Plug 'kristijanhusak/vim-dadbod-ui'

    " automagic window resizer
    " Plug 'camspiers/lens.vim'

    Plug 'bakpakin/janet.vim'

    Plug 'LnL7/vim-nix'

    if has('nvim')
        " Don't remove these two
        Plug 'nvim-lua/plenary.nvim'
        Plug 'antoinemadec/FixCursorHold.nvim'

        Plug 'mrjones2014/smart-splits.nvim'

        Plug 'lukas-reineke/indent-blankline.nvim'
        " Plug 'nathanaelkane/vim-indent-guides'

        Plug 'neovim/nvim-lspconfig'

        " Plug 'hrsh7th/nvim-cmp'
        Plug 'yioneko/nvim-cmp', {'branch': 'perf'}
        Plug 'hrsh7th/cmp-buffer'
        Plug 'hrsh7th/cmp-nvim-lsp'
        Plug 'hrsh7th/cmp-nvim-lua'
        Plug 'hrsh7th/cmp-path'
        Plug 'ray-x/cmp-treesitter'
        Plug 'andersevenrud/cmp-tmux'
        Plug 'PaterJason/cmp-conjure'
        Plug 'hrsh7th/cmp-nvim-lua'
        Plug 'lukas-reineke/cmp-rg'

        Plug 'williamboman/mason.nvim'
        Plug 'williamboman/mason-lspconfig.nvim'
        Plug 'hrsh7th/cmp-nvim-lsp-signature-help'
        Plug 'j-hui/fidget.nvim' " lsp loader eye-candy
        Plug 'stevearc/conform.nvim'
        Plug 'mfussenegger/nvim-lint'

        " Plug 'axvr/zepl.vim'

        " Plug 'github/copilot.vim'
        Plug 'zbirenbaum/copilot.lua'
        " Plug 'zbirenbaum/copilot-cmp'
        " Plug 'samodostal/copilot-client.lua'
        " Plug 'TabbyML/vim-tabby'
        " Plug 'gptlang/CopilotChat.nvim'
        Plug 'CopilotC-Nvim/CopilotChat.nvim', { 'branch': 'canary' }

        " Plug 'nvim-lua/popup.nvim'
        Plug 'nvim-telescope/telescope.nvim'

        Plug 'ibhagwan/fzf-lua'

        " Plug 'jonsmithers/vim-html-template-literals'
        " Plug 'numirias/semshi', {'do': ':UpdateRemotePlugins'}

        " Jupyter stuff
        " Plug 'bfredl/nvim-ipy'
        let g:jupyter_mapkeys = 0
        " Plug 'jupyter-vim/jupyter-vim'

        " Plug 'Olical/conjure', {'tag': 'v4.23.0'}
        Plug 'norcalli/nvim.lua'
        Plug 'Olical/conjure', {'branch': 'develop'}
        " Plug 'Olical/aniseed', { 'tag': 'develop' }
        Plug 'Olical/nfnl'

        Plug 'bakpakin/fennel.vim'
        " Plug 'tami5/compe-conjure'
        Plug 'gpanders/nvim-parinfer'

        " Plug 'sevanteri/vim-gdscript3'

        Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
        " Plug 'nvim-treesitter/playground'
        Plug 'nvim-treesitter/nvim-treesitter-textobjects'
        Plug 'mrjones2014/nvim-ts-rainbow'
        Plug 'JoosepAlviste/nvim-ts-context-commentstring'
        Plug 'nvim-treesitter/nvim-treesitter-context'

        " Plug 'andymass/vim-matchup'

        Plug 'nvim-orgmode/orgmode'

        Plug 'L3MON4D3/LuaSnip', {'do': 'make install_jsregexp'}
        Plug 'saadparwaiz1/cmp_luasnip'

        Plug 'mfussenegger/nvim-dap'

        Plug 'echasnovski/mini.nvim'

        Plug 'posva/vim-vue'

        Plug 'tzachar/local-highlight.nvim'

        Plug 'nvim-neotest/nvim-nio'
        Plug 'nvim-neotest/neotest'
        Plug 'nvim-neotest/neotest-python'

        Plug 'rlane/pounce.nvim'

    endif
endif

call plug#end()
