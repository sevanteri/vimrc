if &diff
    nnoremap <silent> <Left> :diffget LOCAL<CR>
    nnoremap <silent> <Right> :diffget REMOTE<CR>
    nnoremap <silent> <Up> :diffget BASE<CR>
    nnoremap <silent> <Down> />>>>>\\|=====\\|<<<<<<CR>zz

endif
