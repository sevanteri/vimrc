" === insert append in text objects ===

nnoremap <expr> <silent> <Leader>a Append()
nnoremap <expr> <silent> <Leader>i Insert()

function! NOP(...)
endfunction

function! Append(type = '', ...)
    if a:type == ''
        set operatorfunc=Append
        return "g@"
    endif

    normal! `]
    if a:type == 'char'
        call feedkeys("a", 'n')
    else
        call feedkeys("o", 'n')
    endif
    " set operatorfunc=NOP
    " normal g@
    " set operatorfunc=Append
endfunction
function! Insert(type = '', ...)
    if a:type == ''
        set operatorfunc=Insert
        return "g@"
    endif

    normal! `[
    if a:type == 'char'
        call feedkeys("i", 'n')
    else
        call feedkeys("O", 'n')
    endif
endfunction


" lua << EOF
" EOF
