if !exists('g:personal_run_these_once')
    syntax on
    set background=dark
    let g:gruvbox_contrast_light='hard'
    colorscheme gruvbox
    " possible true colors
    if exists('+termguicolors')
      let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
      let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
      set termguicolors
    endif

    let g:personal_run_these_once = 1
endif


" netrw
" nnoremap <leader>e :Lexplore<cr>
let g:netrw_liststyle=3
let g:netrw_banner = 0
let g:netrw_winsize = 16
let g:netrw_browse_split = 4

" nnoremap <leader>p :Files<cr>
" nnoremap <leader>m :History<cr>
" nnoremap <silent> <leader>b :Buffers<cr>
" imap <c-x><c-f> <plug>(fzf-complete-path)
" imap <c-x><c-l> <plug>(fzf-complete-line)
" nmap <leader><tab> <plug>(fzf-maps-n)
" xmap <leader><tab> <plug>(fzf-maps-x)
" omap <leader><tab> <plug>(fzf-maps-o)
"imap <leader><tab> <plug>(fzf-maps-i)

" fugitive
autocmd BufNewFile,BufRead fugitive://* set bufhidden=delete
nnoremap <leader>G :tab G \| -1tabmove<cr>
nnoremap <leader>g :tab G \| -1tabmove<cr>

if executable('rg')
  " Use gg over grep
  set grepprg=rg\ --vimgrep
elseif executable('ag')
  " Use ag over grep
  set grepprg=ag\ --nogroup\ --nocolor
endif


" ==== tbone ====
nnoremap <leader>LL :%Twrite {bottom-right} <CR>
vnoremap <leader>L  :Twrite {bottom-right} <CR>
nnoremap <leader>L  :set opfunc=ObjTWrite <CR>g@
function! ObjTWrite(type, ...)
    call feedkeys(":'[,']Twrite {bottom-right}\n")
endfunction

" ==== easy align ====
" Start interactive EasyAlign in visual mode (e.g. vipga)
" xmap ga <Plug>(EasyAlign)
" xmap gla <Plug>(LiveEasyAlign)
" " Start interactive EasyAlign for a motion/text object (e.g. gaip)
" nmap ga <Plug>(EasyAlign)
" nmap gla <Plug>(LiveEasyAlign)
" " xmap gap :EasyAlign *,<cr>
" xmap gap :EasyAlign *\  {'lm':1}<cr>
" xmap gsap :EasyAlign *,<cr>
" nmap gap :set opfunc=AlignGap<CR>g@
" function! AlignGap(type, ...)
"     let [l1, l2] = ["'[", "']"]
"     let range = l1.','.l2
"     " execute range . "call easy_align#align(0, 0, '', '*,\\ {''lm'': 0, ''stl'': 1}')"
"     execute range . "call easy_align#align(0, 0, '', '*\\ {''lm'': 1}')"
" endfunction



" tagbar
nmap <F8> :TagbarToggle<CR>


" === indent guides
let g:indent_guides_guide_size = 1
let g:indent_guides_start_level = 2
let g:indent_guides_auto_colors = 0
let g:indent_guides_enable_on_vim_startup = 1
"autocmd VimEnter,Colorscheme * :hi IndentGuidesOdd  guibg=red ctermbg=0
"autocmd VimEnter,Colorscheme * :hi IndentGuidesEven guibg=green ctermbg=0


" ====== quickscope =====
" https://gist.github.com/cszentkiralyi/dc61ee28ab81d23a67aa
let g:qs_enable = 0
let g:qs_enable_char_list = [ 'f', 'F', 't', 'T' ]

function! Quick_scope_selective(movement)
    let needs_disabling = 0
    if !g:qs_enable
        QuickScopeToggle
        redraw
        let needs_disabling = 1
    endif
    let letter = nr2char(getchar())
    if needs_disabling
        QuickScopeToggle
    endif
    return a:movement . letter
endfunction

for i in g:qs_enable_char_list
	execute 'noremap <expr> <silent>' . i . " Quick_scope_selective('". i . "')"
endfor


" ===tmux====
let g:tmux_navigator_no_mappings = 1

nnoremap <silent> <M-h> :TmuxNavigateLeft<cr>
nnoremap <silent> <M-j> :TmuxNavigateDown<cr>
nnoremap <silent> <M-k> :TmuxNavigateUp<cr>
nnoremap <silent> <M-l> :TmuxNavigateRight<cr>


" ==== rest ====
let g:vrc_trigger = '<localleader>r'
let g:vrc_split_request_body = 0
let g:vrc_curl_opts = {
  \ '-s': '',
  \ '-i': '',
  \ '-k': '',
\}
let g:vrc_auto_format_response_enabled = 1
let s:vrc_auto_format_response_patterns = {
      \ 'json': 'jq .',
    \}



" === ELM ====
let g:elm_syntastic_show_warnings = 1
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1


" sandwich
" vim-surround keymaps
runtime macros/sandwich/keymap/surround.vim
" in middle of text obj
xmap im <Plug>(textobj-sandwich-literal-query-i)
xmap am <Plug>(textobj-sandwich-literal-query-a)
omap im <Plug>(textobj-sandwich-literal-query-i)
omap am <Plug>(textobj-sandwich-literal-query-a)


" vim-html-template-literals
let g:htl_all_templates=1


" lens
let g:lens#width_resize_min = 80
let g:lens#width_resize_max = 100


" vimwiki
let g:vimwiki_folding = 'expr'
let g:vimwiki_list = [{'path': '~/vimwiki/', 'syntax': 'markdown', 'ext': '.md'}]
                    " \{'path': '~/vimwiki/perhe/'},
                    " \]


let g:taskwiki_markup_syntax = "markdown"


let g:any_jump_disable_default_keybindings = 1


" let g:matchup_delim_noskips = 1   " recognize symbols within comments
let g:matchup_delim_noskips = 2   " don't recognize anything in comments
let g:matchup_matchparen_offscreen = {'method': 'popup'}


""" LUA

if has('nvim')
    let g:conjure#extract#tree_sitter#enabled = v:true
    let g:conjure#mapping#doc_word = "gk"
endif




" neovide
set guifont=Hack:h9
let g:neovide_cursor_animation_length=0.02
let g:neovide_cursor_antialiasing=v:false

" " treesitter-unit
" xnoremap <silent> iu :lua require"treesitter-unit".select()<CR>
" xnoremap <silent> au :lua require"treesitter-unit".select(true)<CR>
" onoremap <silent> iu :<c-u>lua require"treesitter-unit".select()<CR>
" onoremap <silent> au :<c-u>lua require"treesitter-unit".select(true)<CR>

" telescope
nnoremap <silent> <leader><tab> :Telescope keymaps<cr>
" Telescope bindings that replace FZF
" nnoremap <silent> <leader>p :Telescope find_files<cr>
" nnoremap <silent> <leader>m :Telescope oldfiles<cr>
" nnoremap <silent> <leader>b :Telescope buffers<cr>
nnoremap <silent> <leader>t :Telescope<cr>
