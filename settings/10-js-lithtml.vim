" https://github.com/jonsmithers/vim-html-template-literals/blob/master/autoload/htl_syntax.vim
"
" there was a variable that made the plugin work on all template strings
" but I'll leave this here anyways

function! LitTemplate()
    if exists('b:current_syntax')
        let s:current_syntax=b:current_syntax
        unlet b:current_syntax
    endif
    syn include @HTMLSyntax syntax/html.vim
    if exists('s:current_syntax')
        let b:current_syntax=s:current_syntax
    endif

    syntax region litHtmlRegion 
                \ contains=@HTMLSyntax,jsTemplateExpression
                \ start=+\(html\)\?`+
                \ skip=+\\`+
                \ end=+`+
                \ extend
                \ keepend

    hi def link litHtmlRegion String

    syn cluster jsExpression add=litHtmlRegion

    syntax region jsTemplateExpressionLitHtmlWrapper 
                \contained 
                \start=+${+ 
                \end=+}+ 
                \contains=jsTemplateExpression 
                \keepend 
                \containedin=htmlString,htmlComment

    syntax region jsTemplateExpressionAsHtmlValue 
                \start=+=[\t ]*${+ 
                \end=++ 
                \contains=jsTemplateExpression 
                \containedin=htmlTag
endfunction

"augroup html-template-literals
"    au!
"    autocmd FileType javascript call LitTemplate()
"    "autocmd FileType javascript,javascript.jsx call htl_indent#amend({'typescript': 0})
"augroup END

