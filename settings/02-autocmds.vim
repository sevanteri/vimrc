autocmd BufLeave *.css,*.less,*scss normal! mC
autocmd BufLeave *.html             normal! mH
autocmd BufLeave *.js               normal! mJ
autocmd BufLeave *.py               normal! mP
autocmd BufLeave vimrc,*.vim        normal! mV

autocmd BufNewFile *.elm 0r $HOME/.vim/skeletons/skel.elm
autocmd BufNewFile *.fnl 0r $HOME/.vim/skeletons/skel.fnl

if has('nvim')
    autocmd TermOpen term://* :setlocal number! rnu!
endif

autocmd BufEnter *.keymap :set ft=dts
