
" nnoremap <leader>cc :CocCommand<cr>
" nnoremap <leader>cd :CocDisable<cr>
" nnoremap <leader>ce :CocEnable<cr>
" nnoremap <leader>cr :CocRestart<cr>
" nnoremap <leader>cl :CocList<cr>
" nnoremap <leader>cs :CocList Symbols<cr>

" nmap <leader>cl <Plug>(coc-diagnostic-next)
" nmap <leader>ch <Plug>(coc-diagnostic-prev)
" nmap <leader>cjd <Plug>(coc-definition)
" nmap <leader>cr <Plug>(coc-references)
" nmap <leader>cren <Plug>(coc-rename)
" nmap <silent> <leader>cu :<C-u>CocList outline<cr>

" nnoremap <silent> K :call <SID>show_documentation()<CR>

" function! s:show_documentation()
"   if (index(['vim','help'], &filetype) >= 0)
"     execute 'h '.expand('<cword>')
"   else
"     call CocAction('doHover')
"   endif
" endfunction

" nnoremap <silent> <leader>ck :call CocActionAsync('showSignatureHelp')<cr>
" " autocmd FileType python nnoremap <silent> <buffer> K :call CocActionAsync('showSignatureHelp')<cr>

" nnoremap <silent> <M-S-h> :CocCommand document.jumpToPrevSymbol<CR>
" nnoremap <silent> <M-S-l> :CocCommand document.jumpToNextSymbol<CR>


" inoremap <silent><expr> <c-space> coc#refresh()
