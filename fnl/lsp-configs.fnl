(local {: autoload} (require :nfnl.module))
(local nvim (require :nvim))

(local lspconfig (autoload :lspconfig))
(local mason (autoload :mason))
(local mason-lspconfig (autoload :mason-lspconfig))

(local bindings
  [[:n "<localleader>ga" vim.lsp.buf.code_action]
   [:v "<localleader>ga" vim.lsp.buf.code_action]
   ; [:n "gD" vim.lsp.buf.declaration]
   [:n "gd" vim.lsp.buf.definition]
   [:n "gsd" #(do (nvim.ex.vsplit) (vim.lsp.buf.definition))]
   [:n "gD" #(do (vim.cmd "tab split") (vim.lsp.buf.definition))]
   [:n "K" vim.lsp.buf.hover]
   [:n "<localleader>gi" vim.lsp.buf.implementation]
   [:n "<localleader>K" vim.lsp.buf.signature_help]
   [:n "<localleader><C-k>" vim.lsp.buf.signature_help]
   [:n "<localleader>D" vim.lsp.buf.type_definition]
   [:n "<localleader>rn" vim.lsp.buf.rename]
   [:n "<localleader>gr" vim.lsp.buf.references]
   [:n "<localleader>sd" vim.lsp.buf.document_symbol]
   [:n "<localleader>sw" vim.lsp.buf.workspace_symbol]
   [:n "<localleader>d" #(vim.diagnostic.open_float 0 {:scope :line})]
   [:n "[d" vim.diagnostic.goto_prev]
   [:n "]d" vim.diagnostic.goto_next]
   [:n "<localleader>lc" vim.diagnostic.setloclist]])

(fn buf-set-keymap [bufnr mode from to callback]
  (nvim.buf_set_keymap
    bufnr mode from to
    {:noremap true :silent true :callback callback}))

(fn bind-lsp-keymaps-to-buffer [bufnr]
  (each [_index value (ipairs bindings)]
    (let [[mode keys action] value]
      (match (type action)
        :string
        (buf-set-keymap bufnr mode keys action nil)
        :function
        (buf-set-keymap bufnr mode keys "" action)))))

(fn _on-attach [_client bufnr]
  "Old on-attach function"
  (bind-lsp-keymaps-to-buffer bufnr))


; (lsp_signature.setup
;  {:bind true
;   :hint_enable true
;   :doc_lines 0
;   :floating_window_above_cur_line true
;   :hint_prefix ""
;   :hint_scheme :String})

(local augroup (nvim.create_augroup "UserLspConfig" {:clear true}))

; Use LspAttach autocmd to bind keymaps to buffer and set some options
(nvim.create_autocmd
  :LspAttach
  {:group augroup
   :callback
   (fn [ev]
     (do
       (set nvim.bo.omnifunc "v:lua.vim.lsp.omnifunc")
       ; (tset nvim.bo :formatexpr "v:lua.vim.lsp.formatexpr()")
       (bind-lsp-keymaps-to-buffer ev.buf)))})

; Enable inlay hints in normal mode
(nvim.create_autocmd
  [:LspAttach :InsertEnter :InsertLeave]
  {:group augroup
   :callback
   (fn [ev]
     (let [enabled (not= "InsertEnter" ev.event)]
       (vim.lsp.inlay_hint.enable enabled {:bufnr ev.buf})))})

; ; Disable virtual_text and underscore for diagnostics
(vim.diagnostic.config {:signs true
                        :underline false
                        :virtual_text false
                        :virtual_lines {:only_current_line true}
                        :update_in_insert false})

(mason.setup {})
(mason-lspconfig.setup {})

(each [_ server (ipairs (mason-lspconfig.get_installed_servers))]
  (match server
    :pyright
    (comment (lspconfig.pyright.setup {}))
    :pylsp
    (lspconfig.pylsp.setup
      {; :flags {:debounce_text_changes 200}
       ; :root_dir (lspconfig.util.root_pattern ".git/" ".venv" "venv")
       :settings {:pylsp
                  {:configurationSources []
                   :plugins {:pycodestyle {:enabled false :maxLineLength 120}
                             :pyflakes {:enabled false}
                             :pydocstyle {:enabled false}
                             :flake8 {:enabled false :maxLineLength 120}
                             :pep8 {:enabled false}
                             :mccabe {:enabled false}
                             :pylint {:enabled false}
                             :pyls_mypy {:enabled false :live_mode false}
                             :ruff {:enabled false :lineLength 120}}}}})
               ; {:analysis {:autoSearchPaths false
               ;             :useLibraryCodeForTypes false
               ;             :diagnosticMode :openFilesOnly}}}})

    :lua
    (lspconfig.lua.setup
     {:settings {"Lua" {:diagnostics {:globals ["vim"]}}}})

    :fennel_language_server
    (lspconfig.fennel_language_server.setup
      {:filetypes ["fennel"]
       :root_dir (lspconfig.util.root_pattern :fnl :lua)
       :single_file_support true
       :settings {:fennel {:diagnostics {:globals [:vim :jit :comment]}}}})

    ; :rust_analyzer
    ; (rust-tools.setup
    ;   {:server {:standalone false
    ;             :settings {:rust-analyzer {:checkOnSave {:command "clippy"}}}}})

    _ ; else
    ((. lspconfig server :setup) {})))

; other servers outside of Mason

(lspconfig.ccls.setup
  {:root_dir (lspconfig.util.root_pattern ".git/" ".ccls" "compile_commands.json")
   :init_options {:compilationDatabaseDirectory "app/build"}})

(lspconfig.gdscript.setup
  {:root_dir (lspconfig.util.root_pattern "project.godot" ".git/")})

(lspconfig.ruff.setup {})

; (lspconfig.basedpyright.setup {})

; Fennel LS
; (tset configs :fennel-ls
;   {:default_config
;    {:cmd "/home/sevanteri/.cargo/bin/fennel-language-server"
;      :filetypes ["fennel"]
;      :root_dir #(lspconfig.util.find_git_ancestor #1)}})
;
; (lspconfig.fennel-ls.setup
;   {:cmd ["/home/sevanteri/.cargo/bin/fennel-language-server"]
;    :filetypes ["fennel"]
;    :root_dir (fn [dir] (lspconfig.util.find_git_ancestor dir))})


{}
