(local {: autoload} (require :nfnl.module))
(local nvim (autoload :nvim))
(local conform (autoload :conform))
(local lint (autoload :lint))

; Formatter stuff
(conform.setup
  {:formatters_by_ft
   {:python ["ruff_format" "ruff_fix"]
    :sql ["sqlfmt"]
    :typescript ["prettier"]}
   :formatters {:sqlfmt {:exe "sqlfmt"
                         :args ["-" "--fast" "--line-length" "120"]}}})

(set vim.o.formatexpr "v:lua.require'conform'.formatexpr()")

(set lint.linters_by_ft {:python ["mypy"]})
(nvim.create_autocmd
  :BufWritePost
  {:group (nvim.create_augroup "UserLint" {})
   :callback (fn [_ev] (lint.try_lint))})


{}
