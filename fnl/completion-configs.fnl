(local {: autoload} (require :nfnl.module))
(local nvim (autoload :nvim))
(local log (autoload :conjure.log))
(local util (autoload :util))
(local cmp (autoload :cmp))
(local luasnip (autoload :luasnip))

(fn rg-keyword-count []
  "'disable' rg when we're in home dir"
  (if (= (nvim.fn.expand "~") (nvim.fn.getcwd))
    999
    4))

(cmp.setup
  {:sources (cmp.config.sources
              [{:name "copilot"}]
              [{:name "nvim_lsp"}
               {:name "conjure"}
               {:name "vim-dadbod-completion"}]
              [{:name "nvim_lua"}
               {:name "luasnip"}
               {:name "orgmode"}
               ; {:name "treesitter"}
               {:name "buffer" :keyword_length 3 :option {:get_bufnrs (fn [] (nvim.list_bufs))}}
               {:name "tmux" :keyword_length 4 :priority -99}]
              [;{:name "nvim_lsp_signature_help"}
               {:name "rg" :keyword_length (rg-keyword-count) :max_item_count 10}])
    :snippet {:expand (fn [args] (luasnip.lsp_expand args.body))}
    :mapping
    {; "<C-g>" (cmp.mapping.confirm {:select true})
     "<C-Space>" (cmp.mapping.complete)
     "<C-n>" (cmp.mapping.select_next_item {:behavior cmp.SelectBehavior.Insert})
     "<C-p>" (cmp.mapping.select_prev_item {:behavior cmp.SelectBehavior.Insert})
     "<Down>" (cmp.mapping.select_next_item {:behavior cmp.SelectBehavior.Select})
     "<Up>" (cmp.mapping.select_prev_item {:behavior cmp.SelectBehavior.Select})
     ; "<CR>" (cmp.mapping.confirm {:behavior cmp.ConfirmBehavior.Replace
     ;                              :select true})
     "<C-e>" (cmp.mapping.scroll_docs 4 [:i :c])
     "<C-y>" (cmp.mapping.scroll_docs -4 [:i :c])}

    :formatting
    {:format
     (fn [entry vim_item]
       (do (set vim_item.menu (.. "[" entry.source.name "]"))
         vim_item))}})


{}
