(local {: autoload} (require :nfnl.module))
(local neotest (autoload :neotest))
(local npython (autoload :neotest-python))
(local util (autoload :util))



(neotest.setup {:adapters [(npython {:args ["-vv" "-s"]})]
                :diagnostic {:enabled true}})

(util.nnoremap "<leader>ntr" neotest.run.run)
(util.nnoremap "<leader>nts" neotest.summary.toggle)
(util.nnoremap "<leader>nto" neotest.output.open)
(util.nnoremap "<leader>nta" neotest.summary.attach)
(util.nnoremap "<leader>ntl" neotest.run.run_last)
