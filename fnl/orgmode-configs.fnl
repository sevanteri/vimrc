(local {: autoload} (require :nfnl.module))
(local nvim (autoload :nvim))
(local parser (autoload :nvim-treesitter.parsers))
(local conf (autoload :nvim-treesitter.configs))
(local orgmode (autoload :orgmode))

; (let [parser-conf (parser.get_parser_configs)]
;   (tset parser-conf :org
;     {:file_type :org
;      :install_info {:url "https://github.com/milisims/tree-sitter-org"
;                     :revision :main
;                     :files ["src/parser.c" "src/scanner.cc"]}}))

(local org-templates
  {:t {:description "Task" :template "* TODO %?\n  %u"}
   :c {:description "Clipboard" :template "* TODO %?\n  %u\n  %x"}})

(orgmode.setup
  {:org_agenda_files ["~/org/*"]
   :org_default_notes_file "~/org/capture.org"
   :mappings
   {:capture {:org_capture_finalize "<leader>w"
              :org_capture_kill "q"}}
   :org_agenda_templates org-templates})


(fn capture-clipboard [args]
  (when args.bang
    ; after the capture buffer is closed, we return to the initial window and just quit.
    (nvim.command "autocmd BufEnter <buffer> sleep 10m | quitall"))

  (orgmode.action :capture.open_template org-templates.c)

  (when args.bang
   ; set the capture window as big as possible
   ; orgmode didn't like `wincmd o`
   (nvim.command "resize")))

(nvim.create_user_command "CaptureClipboard" capture-clipboard {:bang true})
