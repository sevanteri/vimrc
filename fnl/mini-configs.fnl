(local {: autoload} (require :nfnl.module))
(local nvim (require :nvim))
(local util (require :util))
(local bufrm (autoload :mini.bufremove))
(local align (autoload :mini.align))
(local splitjoin (autoload :mini.splitjoin))
(local cmnt (autoload :mini.comment))
(local minimap (autoload :mini.map))
(local diff (autoload :mini.diff))
(local operators (autoload :mini.operators))
(local sessions (autoload :mini.sessions))
(local starter (autoload :mini.starter))
(local files (autoload :mini.files))
(local ctx_cmntstr (autoload :ts_context_commentstring.internal))
(local bracketed (autoload :mini.bracketed))

(bracketed.setup {:oldfile {:suffix ""}})

(starter.setup {:evaluate_single true
                :items
                [(starter.sections.builtin_actions)
                 (starter.sections.sessions)
                 (starter.sections.recent_files 10 true)]})

(sessions.setup {:directory "~/.vim/sessions"})
(operators.setup {})

(splitjoin.setup)

(diff.setup {:view
             {:style "sign"
              :signs {:add "▎" :change  "▎" :delete  ""}
              :priority 0}})
(util.nnoremap "<leader>hs" ":norm ghgh<CR>" {:silent true})
(util.nnoremap "<leader>hr" ":norm gHgh<CR>" {:silent true})
(util.nnoremap "<leader>ht" diff.toggle_overlay)

(bufrm.setup {})
(util.nnoremap "<leader>d" bufrm.delete)

(align.setup {})

(cmnt.setup
  {:options {:ignore_blank_line true}
   :hooks {:pre (fn [] (ctx_cmntstr.update_commentstring))}})

(files.setup {})
(util.nnoremap "<leader>e" #(files.open (nvim.buf_get_name 0)))
(util.nnoremap "<leader>E" #(files.open (nvim.buf_get_name 0) false))

; (minimap.setup {:integrations [(minimap.gen_integration.builtin_search)
;                                (minimap.gen_integration.diagnostic)
;                                (minimap.gen_integration.gitsigns)]
;                 :symbols {:encode (minimap.gen_encode_symbols.dot "3x2")
;                           :scroll_line "┃"}
;                 :window {:show_integration_count false
;                          :winblend 50}})

; (each [_ value (ipairs [[:n "<Leader>mt" minimap.toggle]])]
;   (let [[mode key action] value]
;     (vim.keymap.set mode key action)))

; (indentscope.setup
;   {:draw {:delay 50 :animation (indentscope.gen_animation "none")}
;    :mappings {:object_scope "ii"
;               :object_scope_with_border "ai"}
;    :options {:border "top"}})

; (surr.setup
;   {:mappings {:add "ys"
;               :delete "ds"
;               :replace "cs"}})
