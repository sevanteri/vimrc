(local {: autoload} (require :nfnl.module))
(local a (autoload :nfnl.core))
(local str (autoload :nfnl.string))
(local log (autoload :conjure.log))
(local nvim (autoload :nvim))

(local output-buffer-name "rest-fnl-output")

(fn set-base-url [url]
  (set _G._HTTP_BASE_URL url))

(fn show-output [bufname output opts]
  (local cur-win (nvim.fn.winnr))
  (local output-win (nvim.fn.bufwinnr (nvim.fn.bufnr bufname)))
  (if (= output-win -1)
    ; create or change to window
    (do
      (nvim.command (.. "rightbelow keepalt vsplit " bufname))
      (set nvim.bo.buftype "nofile"))
    (nvim.command (.. output-win "wincmd w")))

  (set nvim.bo.modifiable true)
  ; clear and fill
  (nvim.command "0,$d _")
  (let [split (nvim.fn.split
                 (nvim.fn.substitute output "[[:return:]]" "" "g")
                 "\n")]
    (nvim.fn.setline "." split))

  (if (. opts :json)
    (set nvim.bo.ft :json)
    (set nvim.bo.ft ""))

  (set nvim.bo.modifiable false)
  (nvim.command (.. cur-win "wincmd w")))

(fn make-url [url]
  (if (and
        _G._HTTP_BASE_URL
        (= "/" (string.sub url 1 1)))
    (.. _G._HTTP_BASE_URL url)
    url))

(fn urlencode [text]
  (-> text
      (string.gsub
        "([^0-9a-zA-Z !'()*._~-])"
        (fn [c] (string.format "%%%02X" (string.byte c))))
      (string.gsub " " "+")))

(fn array-to-querystring [key array]
  (->>
    (icollect [_ v (ipairs array)] (.. key "=" (urlencode v)))
    (str.join "&")))

(fn to-querystring [tbl]
  (if (a.nil? tbl)
    ""
    (->>
      (icollect [key value (pairs tbl)]
        (if (a.table? value)
          (array-to-querystring key value)
          (.. key "=" (urlencode value))))
      (str.join "&"))))

(fn to-json [tbl]
  (nvim.fn.json_encode tbl))

(fn from-json [json]
  (nvim.fn.json_decode json))

(fn to-headers [tbl]
  (if (a.nil? tbl)
    ""
    (->>
      (icollect [k v (pairs tbl)]
                (string.format "%s: %s" k v))
      (a.map nvim.fn.shellescape)
      (str.join " -H ")
      (.. "-H "))))

(fn to-curl [method url data]
  (local data (or data {}))
  (str.join
    " " ["curl" "-s"
         "-X" (string.upper method)

         (let [body (. data :body)]
           (if body
             (.. " -d "
                 (nvim.fn.shellescape (to-json body)))))

         (let [headers (. data :headers)]
           (to-headers headers))

         (if (. data :json)
           "-H 'Content-Type:application/json' -H 'Accept:application/json'")

         (nvim.fn.shellescape
           (.. (make-url url)
             (let [query (. data :query)]
               (if query
                 (.. "?"
                     (to-querystring query))
                 ""))))]))

(var last-output nil)

(fn get-output []
  last-output)

(fn job-output [data]
  (fn [ch output name]
    (if (. data :log) (log.append output))
    (let [output-str (str.join "\n" output)]
      (if (. data :save_output)
        (set last-output (from-json output-str)))
      (show-output output-buffer-name
                   output-str
                   data))))

(fn http [method url data]
  (local data (or data {}))
  (var cmd (to-curl method url data))
  (if (. data :json)
    (set cmd (.. cmd "| jq .")))
  (if (. data :sync)
    (do
      (show-output output-buffer-name
                   (nvim.fn.system cmd)
                   data))
    (nvim.fn.jobstart
      cmd
      {:stdout_buffered true
       :on_stdout (job-output data)
       :stderr_buffered true})))

(fn delete [url data]
  (http :delete url data))

(fn put [url data]
  (http :put url data))

(fn post [url data]
  (http :post url data))

(fn get [url data]
  (http :get url data))

(local json
  {:get (fn [url data] (get url (a.merge data {:json true})))
   :put (fn [url data] (put url (a.merge data {:json true})))
   :post (fn [url data] (post url (a.merge data {:json true})))
   :delete (fn [url data] (delete url (a.merge data {:json true})))})


{: json
 : set-base-url
 : get
 : post
 : put
 : delete}

