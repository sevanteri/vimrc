(local {: autoload} (require :nfnl.module))
(local nvim (autoload :nvim))
(local sitter (autoload :nvim-treesitter.configs))
(local context (require :treesitter-context))

(sitter.setup
  {:ensure_installed ["python" "typescript" "fennel" "org" "markdown" "c" "yaml" "vimdoc"]
   :highlight {:enable true
               :disable [ "org"]
               :additional_vim_regex_highlighting ["org"]}
   :indent {:enable false
            :disable ["python"]}
   :incremental_selection
   {:enable true
    :keymaps {:init_selection "<leader>n"
              :node_incremental "<M-+>"
              :node_decremental "<M-->"
              :scope_incremental "<leader>tsi"}}
   :textobjects {:select {:enable true
                          :lookahead true
                          :keymaps
                          {"af" "@function.outer"
                           "if" "@function.inner"
                           "aco" "@conditional.outer"
                           "ico" "@conditional.inner"
                           "aca" "@call.outer"
                           "ica" "@call.inner"
                           "acl" "@class.outer"
                           "icl" "@class.inner"
                           "aa" "@parameter.outer"
                           "ia" "@parameter.inner"
                           "as" "@statement.outer"
                           "ae" "@expression.outer"}}
                 :move {:enable true
                        :goto_next_start
                          {"<C-Down>" "@statement.outer"}
                        :goto_previous_start
                          {"<C-Up>" "@statement.outer"}}
                 :swap {:enable true
                        :swap_next
                          {"<S-Down>" "@statement.outer"}
                        :swap_previous
                          {"<S-Up>" "@statement.outer"}}}
   :textsubjects {:enable true
                  :keymaps
                  {"<cr>" "textsubjects-smart"}}
   :query_linter {:enable false
                  :use_virtual_text true
                  :lint_events ["BufWrite" "CursorHold"]}
   :rainbow {:enable false}
   :matchup {:enable true}
   :context_commentsring {:enable true
                          :enable_autocmd false}})
   ; :refactor {:highlight_definitions {:enable true}
   ;            :highlight_current_scope {:enable false}
   ;            :smart_rename {:enable true
   ;                           :keymaps {:smart_rename "gren"}}}})

(context.setup
  {:multiline_threshold 1})
