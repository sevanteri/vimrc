(local {: autoload} (require :nfnl.module))
(local nvim (autoload :nvim))
(local log (autoload :conjure.log))
(local tsu (autoload :nvim-treesitter.ts_utils))
(local tsq (autoload :vim.treesitter.query))
(local queries (autoload :nvim-treesitter.query))


; (local node (tsu.get_node_at_cursor) :foo)
; (tsu.get_node_text node)
; (tsu.get_vim_range [(tsu.get_node_range node)])
; (tsu.get_node_text (get-form-node node))
; (slarf node :out :right)

; (fn get-form-node [node]
;   (when node
;     (let [first-child (node:child 0)]
;       (if (and
;             first-child
;             (= (first-child:type) "("))
;         node
;         (get-form-node (node:parent))))))


(fn get-form-node [node]
  "Get the ancestor form node that is the slurper/barfer."
  (let [matches (queries.get_capture_matches bufnr "@form" "slarf" node)]
    (if (a.empty? matches)
      (get-form-node (node:parent))
      node)))

(fn text-in-range [row col end-row end-col]
  (let [row (a.inc row)
        col (a.inc col)
        line (vim.fn.getline row)]
    (string.sub line col end-col)))

(fn node-range-with-space [node side]
  (let [(row col row-end col-end) (tsu.get_node_range node)
        new-col (a.dec col)
        new-col-end (a.inc col-end)
        node-str (match side
                   :left (text-in-range row col row-end new-col-end)
                   :right (text-in-range row new-col row-end col-end))
        space (match side
                :left (string.sub node-str -1)
                :right (string.sub node-str 1 1))]
    (if (= " " space)
      (match side
        :left (values row col row-end new-col-end)
        :right (values row new-col row-end col-end)))))


(fn slarf [node dir side]
  (let [bufnr (nvim.buf.nr)
        slarf_node (get-form-node node)
        paren_node (match side
                     :left (slarf_node:child 0)
                     :right (slarf_node:child (a.dec (slarf_node:child_count))))
        node_to_move (match dir
                       :in (match side
                             :left (tsu.get_previous_node slarf_node)
                             :right (tsu.get_next_node slarf_node))
                       :out (match side
                              :left (slarf_node:child 1)
                              :right (slarf_node:child (- (slarf_node:child_count) 2))))]
    (when node_to_move
      (let [range [(node-range-with-space node_to_move side)]]
        (when range
          (match dir
            :in (tsu.swap_nodes paren_node range)
            :out (tsu.swap_nodes range paren_node)))))))


(fn slurp_left [] (slarf (tsu.get_node_at_cursor) :in :left))
(fn slurp_right [] (slarf (tsu.get_node_at_cursor) :in :right))

(fn barf_left [] (slarf (tsu.get_node_at_cursor) :out :left))
(fn barf_right [] (slarf (tsu.get_node_at_cursor) :out :right))



(fn setup []
  (let [mapping [["SlarfBarfLeft" "barf_left"]
                 ["SlarfBarfRight" "barf_right"]
                 ["SlarfSlurpLeft" "slurp_left"]
                 ["SlarfSlurpRight" "slurp_right"]]]

    (each [_ [map com] (ipairs mapping)]
       (let [mapping (.. "<Plug>(" map ")")
             command (.. ":lua require'treesitter-slarf'." com "()<cr>")]
         (nvim.set_keymap :n mapping command {:noremap true :silent true}))))


  (tsq.set_query "fennel" "slarf" "(_ . \"(\" \")\" .) @form"))
