(local {: autoload} (require :nfnl.module))
(local nvim (autoload :nvim))
(local util (autoload :util))

(let [config-files [:completion-configs
                    :lsp-configs
                    :orgmode-configs
                    :treesitter-configs
                    :mini-configs
                    :neotest-configs
                    :telescope-configs
                    :format-lint-configs]]
  (each [_ config (ipairs config-files)]
    (require config)))

;; plugins
(local ibl (autoload :ibl)) ; indent-blankline...
(local fidget (autoload :fidget))
(local copilot (autoload :copilot))
(local copilot-chat (autoload :CopilotChat))
(local smart-splits (autoload :smart-splits))
(local pounce (autoload :pounce))
(local local-highlight (autoload :local-highlight))
(local fzf-lua (autoload :fzf-lua))

; auto disable error highlight in insert mode
(local auto_error_hl (nvim.create_namespace "auto_error_hl"))
(nvim.set_hl auto_error_hl "@error" {})
(local auto_error_ns (nvim.create_augroup "auto_error_hl" {:clear true}))
(nvim.create_autocmd
  [:InsertEnter]
  {:group auto_error_ns
   :callback (fn [] (nvim.win_set_hl_ns (nvim.get_current_win) auto_error_hl))})
(nvim.create_autocmd
  [:InsertLeave]
  {:group auto_error_ns
   :callback (fn [] (nvim.win_set_hl_ns (nvim.get_current_win) 0))})


(local-highlight.setup {})

(do
  (pounce.setup {})
  (util.nnoremap "<BS>" pounce.pounce))


(do
  (smart-splits.setup {})
  ; smart-split keymaps
  (util.nnoremap "<C-M-h>" smart-splits.resize_left)
  (util.nnoremap "<C-M-j>" smart-splits.resize_down)
  (util.nnoremap "<C-M-k>" smart-splits.resize_up)
  (util.nnoremap "<C-M-l>" smart-splits.resize_right)
  (util.nnoremap "<M-h>" smart-splits.move_cursor_left)
  (util.nnoremap "<M-j>" smart-splits.move_cursor_down)
  (util.nnoremap "<M-k>" smart-splits.move_cursor_up)
  (util.nnoremap "<M-l>" smart-splits.move_cursor_right))

(fidget.setup {})

(ibl.setup
  {:scope {:enabled true :show_start false :show_end false}})
   ; :show_current_context_start true})


(do  ; FZF setup
  (fn fzf-prox-files []
    (local current-file-path (vim.fn.expand "%"))
    (fzf-lua.files {:cmd (.. "proximity-fdfind " current-file-path)}))

  (fzf-lua.setup
    {:winopts {:width 1 :height 0.5 :row 1}

     :actions {:buffers {"ctrl-d" {:fn fzf-lua.actions.buf_del :reload true}
                         "default" fzf-lua.actions.buf_edit
                         "ctrl-s" fzf-lua.actions.buf_split
                         "ctrl-v" fzf-lua.actions.buf_vsplit
                         "ctrl-t" fzf-lua.actions.buf_tabedit}}})

  (do
    (util.nnoremap "<leader>p" fzf-prox-files)
    (util.nnoremap "<leader>m" fzf-lua.oldfiles)
    (util.nnoremap "<leader>b" fzf-lua.buffers)))

(copilot.setup
  {:suggestion {:auto_trigger true
                :keymap {:accept_word "<C-f>"
                         :accept "<C-g>"}}
   :filetypes {:help false
               :text true
               :org false
               :TelescopePrompt false}
   :plugin_manager_path (.. vim.env.HOME "/.vim/bundle")})

(copilot-chat.setup {})

; (copilot-cmp.setup {:method "getCompletionsCycling"})

; (copilot-client.setup {:mapping
;                        {:accept "<CR>"}})

; (nvim.set_keymap :i "<C-c>" "" {:noremap true :silent true :callback copilot-client.suggest})

{}
