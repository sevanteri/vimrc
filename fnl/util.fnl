(local {: autoload} (require :nfnl.module))
(local a (autoload :nfnl.core))
(local nvim (autoload :nvim))

(fn noremap [mode from to opts]
  (match (type to)
    :string
    (nvim.set_keymap
      mode from to
      (a.merge opts {:noremap true}))
    :function
    (nvim.set_keymap
      mode from ""
      (a.merge opts {:noremap true :callback to}))))

(fn nnoremap [from to opts]
  (noremap :n from to opts))

(fn inoremap [from to opts]
  (noremap :i from to opts))

{: nnoremap
 : noremap
 : inoremap}
