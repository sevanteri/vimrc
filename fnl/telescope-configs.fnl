(local {: autoload} (require :nfnl.module))
(local telescope (autoload :telescope))
(local ts-sorters (autoload :telescope.sorters))
(local ts-fzy (autoload :telescope.algos.fzy))
(local themes (autoload :telescope.themes))
(local nvim (autoload :nvim))

(local order-preserving-sorter
  (: ts-sorters.Sorter :new
     {:scoring_function (fn [_ prompt line]
                          (if (not (ts-fzy.has_match prompt line)) -1 1))
      :highlighter (fn [_ prompt display]
                     (ts-fzy.positions prompt display))}))

(telescope.setup
  {:defaults (themes.get_ivy {})
   :pickers
   {:find_files {:theme :ivy
                 :find_command
                  (if (nvim.fn.executable "proximity-fdfind")
                    (fn [opts]
                      (let [file (vim.fn.expand "%")]
                        (if file
                          ["proximity-fdfind" file]
                          ["proximity-fdfind" "." "--strip-cwd-prefix"]))))
                  :sorter order-preserving-sorter
                  :tiebreak
                  (fn [current existing prompt]
                    "just return false so that the order is preserved"
                    false)}
    :buffers {:sort_lastused true
              :ignore_current_buffer true
              :sort_mru true
              :theme :ivy
              :mappings {:i {"<C-d>" "delete_buffer"}
                         :n {"<C-d>" "delete_buffer"}}}}})

{}
