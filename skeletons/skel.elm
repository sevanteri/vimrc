module Main exposing (..)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)

type alias Model =
    { count: Int
    }


model : Model
model =
    { count = 0
    }


type Msg
        = Increase
        | Decrease
        | Reset


update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
    case msg of
        Increase ->
            { model | count = model.count + 1 } ! []
        Decrease ->
            { model | count = model.count - 1 } ! []
        Reset ->
            { model | count = 0 } ! []


view : Model -> Html Msg
view model =
    div [ style [ ("padding", "2rem") ] ]
        [ text <| "Current count: " ++ toString model.count
        , button [ onClick Increase ] [ text "Increase" ]
        ]

main : Program Never Model Msg
main =
    App.program
        { init = ( model, Cmd.none )
        , view = view
        , subscriptions = \_ -> Sub.none
        , update = update
        }
